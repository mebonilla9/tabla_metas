const columnas = [
  {
    "idMeta": 1,
    "nombre": "BANDA ANCHA RESTO"
  },
  {
    "idMeta": 2,
    "nombre": "BANDA ANCHA UBB (>=20 MBPS)"
  },
  {
    "idMeta": 3,
    "nombre": "BANDA ANCHA FIBRA OPTICA"
  },
  {
    "idMeta": 4,
    "nombre": "LINEA BASICA"
  },
  {
    "idMeta": 5,
    "nombre": "TELEVISION"
  },
  {
    "idMeta": 6,
    "nombre": "VOZ"
  },
  {
    "idMeta": 7,
    "nombre": "Datos"
  },
  {
    "idMeta": 8,
    "nombre": "POSTPAGO"
  },
  {
    "idMeta": 9,
    "nombre": "POSTPAGO PYMES"
  },
  {
    "idMeta": 10,
    "nombre": "POSTPAGO INDIVIDUAL"
  },
  {
    "idMeta": 11,
    "nombre": "PREPAGO TEU"
  },
  {
    "idMeta": 12,
    "nombre": "TRANSFER IN"
  },
  {
    "idMeta": 13,
    "nombre": "SEGURO MOVIL"
  },
  {
    "idMeta": 14,
    "nombre": "NAPSTER"
  },
  {
    "idMeta": 15,
    "nombre": "NIVEL DE SERVICIO"
  },
  {
    "idMeta": 18,
    "nombre": "POSTPAGO INDIVIDUAL"
  }
];

export { columnas };
