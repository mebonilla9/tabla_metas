import React, { Component } from "react";

import 'react-table/react-table.css'
import ReactTable from 'react-table';
import withFixedColumns from 'react-table-hoc-fixed-columns';
const ReactTableFixedColumns = withFixedColumns(ReactTable);
import axios from 'axios';

let validaciones = {};

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      periodo: '1810',
      data: [],
      habilitado: false,
      columnasTabla: [],
      habilitado: false
    }
    this.onValorAsignadoChange = this.onValorAsignadoChange.bind(this);
  }

  componentDidMount() {    
    axios.get('https://186.116.14.117:8080/planeacioncomercial/rest/meta/consultar',{headers:{'Authorization':localStorage.token}})
    .then(res => {
      const columnas = res.data;
      console.log(this.state);
      this.setState({
        columnasTabla: [
          ...this.obtenerColumnasFijas(),
          ...this.obtenerColumnasMetas(columnas)
        ]
      });
    });
    axios.get('https://186.116.14.117:8080/planeacioncomercial/rest/metausuario/consultar',{headers:{'Authorization':localStorage.token}})
    .then(res => {
      console.log(this.state);
      this.setState({data: res.data})
    });
  }

  obtenerColumnasFijas = () => {
    return [
      {
        Header: 'Punto',
        accessor: 'infoRol.oficina.nombrePunto',
        fixed: 'left'
      },
      {
        Header: 'Cedula',
        accessor: 'usuario.cedula',
        fixed: 'left'
      },
      {
        Header: 'Nombre',
        accessor: 'usuario.nombre',
        fixed: 'left'
      },
      {
        Header: 'Rol',
        accessor: 'infoRol.rol.nombreRol',
        fixed: 'left'
      },
      {
        Header: Incapacidad,
        fixed: 'left',
        columns: [
          {
            Header: "Fecha ini",
            accessor: 'novedad.fechaInicio',
          },
          {
            Header: "Fecha fin.",
            accessor: 'novedad.fechaFinal',
          }
        ]
      }
    ];
  };

  onValorAsignadoChange = event => {
    let datosTemporales = [...this.state.data];
    const idUsuario = Number(event.currentTarget.attributes['data-usuario'].value);
    const idMeta = Number(event.currentTarget.attributes['data-meta'].value);
    const nuevoValor = Number(event.currentTarget.value);

    const index = datosTemporales.findIndex(d => d.usuario.idUsuario === idUsuario);
    const indexMeta = datosTemporales[index].infoMetas.findIndex(m => m.idMeta === idMeta);
    datosTemporales[index].infoMetas[indexMeta].valorMetaAsig = nuevoValor;

    this.setState({
      data: datosTemporales
    })
  };

  renderMeta = (obj) => {
    if(obj.original.infoMetas == null){
      return <span>-</span>;
    }
    const meta = obj.original.infoMetas.find(meta => meta.idMeta === obj.column.idMeta);
    const campo = obj.column.campo;
    if (meta) {
      if (campo === 'valorMetaAsig') {
        const disabled = meta.valorMetaEsp === 0;
        return (
          <input
            data-usuario={obj.original.usuario.idUsuario}
            data-meta={obj.column.idMeta}
            value={meta[campo]} disabled={disabled}
            onChange={this.onValorAsignadoChange}
          />
        );
      }
      return <span>{meta[campo]}</span>;
    }
    return <span>-</span>;
  };

  sumarCampos = (obj) => {
    const sumatoria = obj.data.reduce((a, b) => {
      if(b._original.infoMetas == null){
        return 0;
      }
      const meta = b._original.infoMetas.find(c => c.idMeta === obj.column.idMeta);
      return a + ((meta) ? meta[obj.column.campo] : 0);
    }, 0);

    const key = `meta_${obj.column.idMeta}`;
    validaciones[key][obj.column.campo] = sumatoria;
    validaciones[key].valido = validaciones[key].valorMetaAsig === validaciones[key].valorMetaEsp;
    return sumatoria;
  };

  validarSumatorias = () => {
    for (const key in validaciones) {
      if (!validaciones[key].valido) {
        this.setState({habilitado: false});
        return;
      }
    }
    this.setState({habilitado: true});
  };

  enviarServidor = () => {
    console.log('Enviando');
    axios.get('https://186.116.14.117:8080/planeacioncomercial/rest/metausuario/cargar',this.state.data, {"Access-Control-Allow-Origin": "*", headers:{'Authorization':localStorage.token}})
    .then(res => {
      console.log(res);
    });
  }

  obtenerColumnasMetas = (columnas) => {
    let objValidaciones = {};
    const columnasTabla = columnas.map(col => {
      objValidaciones[`meta_${col.idMeta}`] = { valorMetaEsp: 0, valorMetaAsig: 0, valido: false };
      return {
        Header: col.nombre,
        accessor: 'usuario.idUsuario',
        columns: [
          {
            Header: "Esp.",
            idMeta: col.idMeta,
            campo: 'valorMetaEsp',
            Cell: this.renderMeta,
            Footer: this.sumarCampos
          },
          {
            Header: "Asign.",
            idMeta: col.idMeta,
            campo: 'valorMetaAsig',
            Cell: this.renderMeta,
            Footer: this.sumarCampos
          }
        ]
      }
    });

    validaciones = {...validaciones, ...objValidaciones};
    return columnasTabla;
  };

  obtenerColumnas = () => {
    return [...this.obtenerColumnasFijas(), ...this.columnasMeta];
  };

  render() {
    if (this.state.columnasTabla.length === 0) {
      return <p>Cargando</p>;
    }

    if(this.state.data.length === 0){
      return <p>Cargando</p>;
    }

    console.log(this.state.data);

    let btnComprobar = null;
    //if (!this.state.habilitado) {
      btnComprobar = <button disabled={this.state.habilitado} onClick={this.validarSumatorias}>Validar sumatorias</button>;
    //}

    let btnEnviar = null
    if (this.state.habilitado) {
      btnEnviar = <button onClick={this.enviarServidor}>Guardar</button>;
    }

    return (
      <div>
        <h1>Metas por coordinador</h1>
        <ReactTableFixedColumns
          data={this.state.data}
          minRows={0}
          columns={this.state.columnasTabla}
        />
        <div>
          {btnComprobar}
        </div>
        <div>
          {btnEnviar}
        </div>
      </div>
    );
  }
}
