const filas = [
  {
    'usuario': {
      'idUsuario': 1,
      'estado': {
        'idEstado': 1,
        'nombreEstado': 'Vigente'
      },
      'cedula': '79890725',
      'correo': 'OSCAR.NIAMPIRA@TELEFONICA.COM',
      'password': null,
      'nombre': 'Oscar Niampira',
      'codInterno': null,
      'idJefe': '345',
      'cargo': {
        'idCargo': 1,
        'estado': {
          'idEstado': 1,
          'nombreEstado': 'Vigente'
        },
        'nomCargo': 'Administrador',
        'datos': 'APP_DATOS_CE'
      },
      'mac': '00:ee:bd:a9:ea:a9',
      'bloqueo': false
    },
    'infoRol': {
      'idDatosce': 3,
      'idUsuariomodifica': 1,
      'rol': {
        'idRol': 1,
        'nombreRol': 'Rol prueba 1'
      },
      'oficina': {
        'idOficina': 1,
        'nombrePunto': 'Punto de prueba',
        'direccion': 'Cra 1 # 1-10',
        'ciudad': 'Bogotá',
        'departamento': 'Bogotá',
        'regional': 'Bogotá',
        'ccCoordinador': 5765
      },
      'novedad': {
        'idNovedad': 1,
        'nombreNovedad': 'Cambio de Punto'
      },
      'fecha': '2018-06-29',
      'idUsuario': 1
    },
    'punto': {
      'idPunto': '4756',
      'nombrePunto': 'Pruebas APP',
      'direccion': 'Av suba 114',
      'ciudad': 'Bogota',
      'departamento': 'Cundinamarca',
      'regional': 'Bogota',
      'idUsuario': 1,
      'idCoordinador': 1
    },
    'infoMetas': [
      {
        'idMetaUsuario': 3,
        'idMeta': 1,
        'valorMetaEsp': 0,
        'valorMetaAsig': 0,
        'idUsuario': 1,
        'periodo': '2018-09'
      },
      {
        'idMetaUsuario': 4,
        'idMeta': 2,
        'valorMetaEsp': 6,
        'valorMetaAsig': 0,
        'idUsuario': 1,
        'periodo': '2018-09'
      },
      {
        'idMetaUsuario': 5,
        'idMeta': 3,
        'valorMetaEsp': 10,
        'valorMetaAsig': 0,
        'idUsuario': 1,
        'periodo': '2018-09'
      },
      {
        'idMetaUsuario': 3,
        'idMeta': 18,
        'valorMetaEsp': 23,
        'valorMetaAsig': 0,
        'idUsuario': 1,
        'periodo': '2018-09'
      }
    ]
  },
  {
    'usuario': {
      'idUsuario': 2,
      'estado': {
        'idEstado': 1,
        'nombreEstado': 'Vigente'
      },
      'cedula': '79890725',
      'correo': 'OSCAR.NIAMPIRA@TELEFONICA.COM',
      'password': null,
      'nombre': 'Oscar Niampira',
      'codInterno': null,
      'idJefe': '345',
      'cargo': {
        'idCargo': 1,
        'estado': {
          'idEstado': 1,
          'nombreEstado': 'Vigente'
        },
        'nomCargo': 'Administrador',
        'datos': 'APP_DATOS_CE'
      },
      'mac': '00:ee:bd:a9:ea:a9',
      'bloqueo': false
    },
    'infoRol': {
      'idDatosce': 3,
      'idUsuariomodifica': 1,
      'rol': {
        'idRol': 1,
        'nombreRol': 'Rol prueba 1'
      },
      'oficina': {
        'idOficina': 1,
        'nombrePunto': 'Punto de prueba',
        'direccion': 'Cra 1 # 1-10',
        'ciudad': 'Bogotá',
        'departamento': 'Bogotá',
        'regional': 'Bogotá',
        'ccCoordinador': 5765
      },
      'novedad': {
        'idNovedad': 1,
        'nombreNovedad': 'Cambio de Punto'
      },
      'fecha': '2018-06-29',
      'idUsuario': 1
    },
    'punto': {
      'idPunto': '4756',
      'nombrePunto': 'Pruebas APP',
      'direccion': 'Av suba 114',
      'ciudad': 'Bogota',
      'departamento': 'Cundinamarca',
      'regional': 'Bogota',
      'idUsuario': 1,
      'idCoordinador': 1
    },
    'infoMetas': [
      {
        'idMetaUsuario': 3,
        'idMeta': 1,
        'valorMetaEsp': 12,
        'valorMetaAsig': 0,
        'idUsuario': 1,
        'periodo': '2018-09'
      },
      {
        'idMetaUsuario': 3,
        'idMeta': 1,
        'valorMetaEsp': 12,
        'valorMetaAsig': 0,
        'idUsuario': 1,
        'periodo': '2018-09'
      },
      {
        'idMetaUsuario': 4,
        'idMeta': 2,
        'valorMetaEsp': 6,
        'valorMetaAsig': 0,
        'idUsuario': 1,
        'periodo': '2018-09'
      },
      {
        'idMetaUsuario': 5,
        'idMeta': 3,
        'valorMetaEsp': 10,
        'valorMetaAsig': 0,
        'idUsuario': 1,
        'periodo': '2018-09'
      },
      {
        'idMetaUsuario': 3,
        'idMeta': 18,
        'valorMetaEsp': 23,
        'valorMetaAsig': 0,
        'idUsuario': 1,
        'periodo': '2018-09'
      }
    ]
  },
  {
    'usuario': {
      'idUsuario': 3,
      'estado': {
        'idEstado': 1,
        'nombreEstado': 'Vigente'
      },
      'cedula': '79890725',
      'correo': 'OSCAR.NIAMPIRA@TELEFONICA.COM',
      'password': null,
      'nombre': 'Oscar Niampira',
      'codInterno': null,
      'idJefe': '345',
      'cargo': {
        'idCargo': 1,
        'estado': {
          'idEstado': 1,
          'nombreEstado': 'Vigente'
        },
        'nomCargo': 'Administrador',
        'datos': 'APP_DATOS_CE'
      },
      'mac': '00:ee:bd:a9:ea:a9',
      'bloqueo': false
    },
    'infoRol': {
      'idDatosce': 3,
      'idUsuariomodifica': 1,
      'rol': {
        'idRol': 1,
        'nombreRol': 'Rol prueba 1'
      },
      'oficina': {
        'idOficina': 1,
        'nombrePunto': 'Punto de prueba',
        'direccion': 'Cra 1 # 1-10',
        'ciudad': 'Bogotá',
        'departamento': 'Bogotá',
        'regional': 'Bogotá',
        'ccCoordinador': 5765
      },
      'novedad': {
        'idNovedad': 1,
        'nombreNovedad': 'Cambio de Punto'
      },
      'fecha': '2018-06-29',
      'idUsuario': 1
    },
    'punto': {
      'idPunto': '4756',
      'nombrePunto': 'Pruebas APP',
      'direccion': 'Av suba 114',
      'ciudad': 'Bogota',
      'departamento': 'Cundinamarca',
      'regional': 'Bogota',
      'idUsuario': 1,
      'idCoordinador': 1
    },
    'infoMetas': [
      {
        'idMetaUsuario': 3,
        'idMeta': 1,
        'valorMetaEsp': 12,
        'valorMetaAsig': 0,
        'idUsuario': 1,
        'periodo': '2018-09'
      },
      {
        'idMetaUsuario': 3,
        'idMeta': 1,
        'valorMetaEsp': 12,
        'valorMetaAsig': 0,
        'idUsuario': 1,
        'periodo': '2018-09'
      },
      {
        'idMetaUsuario': 4,
        'idMeta': 2,
        'valorMetaEsp': 6,
        'valorMetaAsig': 0,
        'idUsuario': 1,
        'periodo': '2018-09'
      },
      {
        'idMetaUsuario': 5,
        'idMeta': 3,
        'valorMetaEsp': 10,
        'valorMetaAsig': 0,
        'idUsuario': 1,
        'periodo': '2018-09'
      },
      {
        'idMetaUsuario': 3,
        'idMeta': 18,
        'valorMetaEsp': 23,
        'valorMetaAsig': 0,
        'idUsuario': 1,
        'periodo': '2018-09'
      }
    ]
  },
  {
    'usuario': {
      'idUsuario': 4,
      'estado': {
        'idEstado': 1,
        'nombreEstado': 'Vigente'
      },
      'cedula': '79890725',
      'correo': 'OSCAR.NIAMPIRA@TELEFONICA.COM',
      'password': null,
      'nombre': 'Oscar Niampira',
      'codInterno': null,
      'idJefe': '345',
      'cargo': {
        'idCargo': 1,
        'estado': {
          'idEstado': 1,
          'nombreEstado': 'Vigente'
        },
        'nomCargo': 'Administrador',
        'datos': 'APP_DATOS_CE'
      },
      'mac': '00:ee:bd:a9:ea:a9',
      'bloqueo': false
    },
    'infoRol': {
      'idDatosce': 3,
      'idUsuariomodifica': 1,
      'rol': {
        'idRol': 1,
        'nombreRol': 'Rol prueba 1'
      },
      'oficina': {
        'idOficina': 1,
        'nombrePunto': 'Punto de prueba',
        'direccion': 'Cra 1 # 1-10',
        'ciudad': 'Bogotá',
        'departamento': 'Bogotá',
        'regional': 'Bogotá',
        'ccCoordinador': 5765
      },
      'novedad': {
        'idNovedad': 1,
        'nombreNovedad': 'Cambio de Punto'
      },
      'fecha': '2018-06-29',
      'idUsuario': 1
    },
    'punto': {
      'idPunto': '4756',
      'nombrePunto': 'Pruebas APP',
      'direccion': 'Av suba 114',
      'ciudad': 'Bogota',
      'departamento': 'Cundinamarca',
      'regional': 'Bogota',
      'idUsuario': 1,
      'idCoordinador': 1
    },
    'infoMetas': [
      {
        'idMetaUsuario': 3,
        'idMeta': 1,
        'valorMetaEsp': 12,
        'valorMetaAsig': 0,
        'idUsuario': 1,
        'periodo': '2018-09'
      },
      {
        'idMetaUsuario': 3,
        'idMeta': 1,
        'valorMetaEsp': 12,
        'valorMetaAsig': 0,
        'idUsuario': 1,
        'periodo': '2018-09'
      },
      {
        'idMetaUsuario': 4,
        'idMeta': 2,
        'valorMetaEsp': 6,
        'valorMetaAsig': 0,
        'idUsuario': 1,
        'periodo': '2018-09'
      },
      {
        'idMetaUsuario': 5,
        'idMeta': 3,
        'valorMetaEsp': 10,
        'valorMetaAsig': 0,
        'idUsuario': 1,
        'periodo': '2018-09'
      },
      {
        'idMetaUsuario': 3,
        'idMeta': 18,
        'valorMetaEsp': 23,
        'valorMetaAsig': 0,
        'idUsuario': 1,
        'periodo': '2018-09'
      }
    ]
  },
  {
    'usuario': {
      'idUsuario': 5,
      'estado': {
        'idEstado': 1,
        'nombreEstado': 'Vigente'
      },
      'cedula': '79890725',
      'correo': 'OSCAR.NIAMPIRA@TELEFONICA.COM',
      'password': null,
      'nombre': 'Oscar Niampira',
      'codInterno': null,
      'idJefe': '345',
      'cargo': {
        'idCargo': 1,
        'estado': {
          'idEstado': 1,
          'nombreEstado': 'Vigente'
        },
        'nomCargo': 'Administrador',
        'datos': 'APP_DATOS_CE'
      },
      'mac': '00:ee:bd:a9:ea:a9',
      'bloqueo': false
    },
    'infoRol': {
      'idDatosce': 3,
      'idUsuariomodifica': 1,
      'rol': {
        'idRol': 1,
        'nombreRol': 'Rol prueba 1'
      },
      'oficina': {
        'idOficina': 1,
        'nombrePunto': 'Punto de prueba',
        'direccion': 'Cra 1 # 1-10',
        'ciudad': 'Bogotá',
        'departamento': 'Bogotá',
        'regional': 'Bogotá',
        'ccCoordinador': 5765
      },
      'novedad': {
        'idNovedad': 1,
        'nombreNovedad': 'Cambio de Punto'
      },
      'fecha': '2018-06-29',
      'idUsuario': 1
    },
    'punto': {
      'idPunto': '4756',
      'nombrePunto': 'Pruebas APP',
      'direccion': 'Av suba 114',
      'ciudad': 'Bogota',
      'departamento': 'Cundinamarca',
      'regional': 'Bogota',
      'idUsuario': 1,
      'idCoordinador': 1
    },
    'infoMetas': [
      {
        'idMetaUsuario': 3,
        'idMeta': 1,
        'valorMetaEsp': 12,
        'valorMetaAsig': 0,
        'idUsuario': 1,
        'periodo': '2018-09'
      },
      {
        'idMetaUsuario': 3,
        'idMeta': 1,
        'valorMetaEsp': 12,
        'valorMetaAsig': 0,
        'idUsuario': 1,
        'periodo': '2018-09'
      },
      {
        'idMetaUsuario': 4,
        'idMeta': 2,
        'valorMetaEsp': 6,
        'valorMetaAsig': 0,
        'idUsuario': 1,
        'periodo': '2018-09'
      },
      {
        'idMetaUsuario': 5,
        'idMeta': 3,
        'valorMetaEsp': 10,
        'valorMetaAsig': 0,
        'idUsuario': 1,
        'periodo': '2018-09'
      },
      {
        'idMetaUsuario': 3,
        'idMeta': 18,
        'valorMetaEsp': 23,
        'valorMetaAsig': 0,
        'idUsuario': 1,
        'periodo': '2018-09'
      }
    ]
  }
];
export { filas };
